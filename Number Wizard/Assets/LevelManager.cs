﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {
	
	public void LoadLevel(string name){
		Debug.Log ("Level log requested for: " + name);
		Application.LoadLevel(name);
	}
	
	public void QuitRequest(){
		Debug.Log ("I Want to quit!");
		Application.Quit ();
	}
}
